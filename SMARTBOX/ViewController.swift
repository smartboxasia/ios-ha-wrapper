//
//  ViewController.swift
//  SMARTBOX
//
//  Created by zynick on 18/12/16.
//  Copyright © 2016 Smart Home Asia Sdn Bhd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: "http://www.smartboxasia.com/showroom.html")
        let request = URLRequest(url: url!)
        webView.loadRequest(request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
    }

    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
}
